window.addEventListener('load', function() {

  // INIT
  fetchData();

  // Fetch API Data
  function fetchData() {
    $.ajax({
      type: 'GET',
      url: 'https://sky-frontend.herokuapp.com/movies',
      success: data => {
        
        renderHighlights(data.find(item => item.type === 'highlights').items);

        renderCarouselPortrait(data.filter(item => item.type === 'carousel-portrait'))

      }
    })
  }


  // HERO SLIDER ~ "HIGHLIGHTS"


  function renderHighlights(highlights) {

    const bannerArea = $('.hero-slider > .swiper-hero > .swiper-wrapper')

    highlights.forEach(item => bannerArea.append(`
      <div class="hero-slider__banner swiper-slide"><img src="${item.images[0].url}" alt="${item.images[0].title}"></div>
    `))

    return initHeroSlider();
  }


  function initHeroSlider() {
    let heroSlider = new Swiper('.swiper-hero', {
      speed: 400,
      spaceBetween: 10,
      slidesPerView: 'auto',
      pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
      },
      breakpoints: {
        600: {
          centeredSlides: true,
          loop: true,
          spaceBetween: 35,
        }
      }
    })
  }
 
  
  // CAROUSEL PORTRAITS

  function renderCarouselPortrait(carousel) {

    // Render Lists

    const mainContent = $('main')
    const titles = carousel.map(item => item.title)

    carousel.forEach(item => mainContent.append(`
      <div class="movie-reel" data-title="${item.title}">
        <h4>${item.title}</h4>

        <div class="swiper-container">

          <div class="swiper-wrapper">

          </div>

        </div>

      </div><!-- end movie-reel-->
    `))


    // Render Movies
    
    titles.forEach(title => renderMovies(title, carousel.find(item => item.title === title ).movies))
    

    return initMovieSlider();
  }

  

  function renderMovies(title, movies) {

      return movies.forEach(movie => $(`div[data-title="${title}"] > .swiper-container > .swiper-wrapper`).append(`
        <div class="movie-reel__movie swiper-slide">
          <img src="${movie.images[0].url}" alt="">
        </div>
      `))
  }


  function initMovieSlider() {
    let movieSlider = new Swiper('.swiper-container', {
      speed: 400,
      loop: true,
      spaceBetween: 10,
      slidesPerView: 'auto',
      breakpoints: {
        600: {
          spaceBetween: 20,
        }
      }
    })
  }
  

  

})

