window.addEventListener('load', () => {

  // TOGGLE MENU
  $('.hamburger').click(toggleMenu);
  $('.close-menu').click(toggleMenu);

  function toggleMenu() {
    $('.header__menu > nav').toggleClass('toggleMenu');
  };

  // TABS
  $('.header-tab__tab').click(handleTabClick);

  function handleTabClick() {
    $('.header-tab__tab').removeClass('header-tab__tab--active')
    $(this).addClass('header-tab__tab--active')
  }

  // TOGGLE FOOTER CORPORATE
  $('.footer-corporate__toggle > span').click(toggleFooterInfo);

  function toggleFooterInfo() {
    $(this).children('i').toggleClass('corporate__toggle--active');
    $(this).siblings('ul').toggleClass('corporate__toggle--active');
  };

} );