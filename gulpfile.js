
const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const del = require('del');
const useref = require('gulp-useref');

const dir = {
  out_dev: './dist',
  src_html: './src/*.html',
  out_html: './dist/',
  src_script: './src/assets/scripts/**/*.js',
  out_script: './dist/assets/scripts',
  name_script: 'main.js',
  src_scss: './src/assets/scss/**/*.scss',
  out_scss: './dist/assets/css',
  src_images: './src/assets/images/**/*.+(png|jpg|jpeg|gif|svg)',
  out_images: './dist/assets/images',
}


function scripts() {
  return gulp.src(dir.src_script)
  .pipe(babel({
    presets: ['@babel/env']
  }))
  .pipe(concat(dir.name_script))
  .pipe(uglify())
  .pipe(gulp.dest(dir.out_script))
  .pipe(browserSync.stream())
}

function styles() {
  return gulp.src(dir.src_scss)
  .pipe(sass())
  .pipe(sass().on('error', sass.logError))
  .pipe(cssnano())
  .pipe(gulp.dest(dir.out_scss))
  .pipe(browserSync.stream())
}

function images() {
  return gulp.src(dir.src_images)
  .pipe(gulp.dest(dir.out_images))
  .pipe(browserSync.stream())
}

function html() {
  return gulp.src(dir.src_html)
    .pipe(gulp.dest(dir.out_dev))
    .pipe(browserSync.stream())
}


function watch() {
  browserSync.init({
    server: {
      baseDir: './dist'
    }
  })
  gulp.watch(dir.src_html, html).on('change', browserSync.reload);
  gulp.watch(dir.src_script, scripts).on('change', browserSync.reload);
  gulp.watch(dir.src_scss, styles);
  gulp.watch(dir.src_images, images).on('change', browserSync.reload);
}

function clearDist() {
  return del('dist');
}

exports.clear = clearDist;
exports.dev = gulp.series(clearDist, scripts, styles, images, html, watch );