# Sky Front end Test

Teste criado com HTML, CSS + SASS, JS + Jquery e uma compilação simples utilizando **Gulp**.

## Como Rodar

- `Git Clone`
- `yarn install`
- `gulp dev`

Autmaticamente a aplicação deve rodar em localhost (normalmente na porta 3000).

## Notas rápidas

Fico devendo:

- Estilização do Footer
- Icones de redes sociais
- Funcionalidades de menu

## Dúvidas, críticas, comentários, uma conf?
[montone@gmail.com](mailto:montone@gmail.com)